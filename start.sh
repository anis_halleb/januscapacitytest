#!/bin/sh -e

READER_PROTOCOL=tcp


LISTENERS_COUNT=
STREAM_URL=
while getopts “c:s:x” OPTION
do
     case $OPTION in
         c)
             LISTENERS_COUNT=$OPTARG
             ;;
         s)
             STREAM_URL=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ -z $STREAM_URL ]]
then
	echo "Missing STREAM_URL (-s parameter)"
    usage
    exit 1
fi
if [[ -z $LISTENERS_COUNT ]]
then
	LISTENERS_COUNT = 10
fi

echo "LISTENERS_COUNT=" $LISTENERS_COUNT
echo "STREAM_URL=" $STREAM_URL
for i in $(seq 1 $LISTENERS_COUNT); do
    echo "STARTING LISTENER $i"
    ffmpeg -hide_banner -loglevel error \
    -rtsp_transport $READER_PROTOCOL \
    -i $STREAM_URL \
    -f mpegts -y /dev/null &
done
